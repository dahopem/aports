# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=flawz
pkgver=0.1.1
pkgrel=0
pkgdesc="A Terminal UI for browsing CVEs"
url="https://github.com/orhun/flawz"
arch="all"
license="MIT OR Apache-2.0"
depends="openssl sqlite"
makedepends="cargo cargo-auditable openssl-dev sqlite-dev"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/orhun/flawz/archive/v$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
	mkdir -p man
	OUT_DIR=man/ target/release/flawz-mangen
	mkdir -p completions
	OUT_DIR=completions/ target/release/flawz-completions
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 "man/$pkgname.1" -t "$pkgdir/usr/share/man/man1"
	install -Dm 644 "completions/$pkgname.bash" "$pkgdir/usr/share/bash-completion/completions/$pkgname"
	install -Dm 644 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d"
	install -Dm 644 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions"
}

sha512sums="
6818f2776bf8c83f65b11471690310e3155e4ef1f25d17466b4410be1d551873cb2aa38513f2d031ead77164ea35c9bc8bd3a77eb8bbf2c061ce45578dd62259  flawz-0.1.1.tar.gz
"
