# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Simon Rupf <simon@rupf.net>
pkgname=xterm
pkgver=391
pkgrel=0
pkgdesc="X Terminal Emulator"
options="!check" # Requires vttest
url="https://invisible-island.net/xterm"
arch="all"
license="MIT"
depends="ncurses-terminfo-base"
makedepends="libxaw-dev libxft-dev ncurses-dev"
subpackages="$pkgname-doc"
source="https://invisible-island.net/archives/xterm/xterm-$pkgver.tgz
	posix-ptys.patch
	"

# secfixes:
#   371-r0:
#     - CVE-2022-24130
#   366-r0:
#     - CVE-2021-27135

build() {
	export CFLAGS="$CFLAGS -flto=auto -D_BSD_SOURCE"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-x \
		--disable-full-tgetent \
		--disable-imake \
		--enable-ansi-color \
		--enable-88-color \
		--enable-256-color \
		--enable-broken-osc \
		--enable-broken-st \
		--enable-load-vt-fonts \
		--enable-i18n \
		--enable-wide-chars \
		--enable-doublechars \
		--enable-warnings \
		--enable-tcap-query \
		--enable-logging \
		--enable-dabbrev \
		--enable-freetype \
		--enable-luit \
		--enable-mini-luit \
		--enable-narrowproto \
		--enable-exec-xterm \
		--enable-sixel-graphics \
		--with-tty-group=tty \
		--with-utempter
	make
}

package() {
	make DESTDIR="$pkgdir" install
	install -m755 -d "$pkgdir"/usr/share/applications
	install -m644 "$builddir"/xterm.desktop "$pkgdir"/usr/share/applications/
	install -m644 "$builddir"/uxterm.desktop "$pkgdir"/usr/share/applications/
}

sha512sums="
60a6bcdb194e37ee716995b4d252ebe2adb9f2b7321936449b877455887c120be3f96dccbc6c7ef60de23286e926a07677780db2ce02ed89da4e39537aa820e1  xterm-391.tgz
44da86e5712b59171d701fea2a7e2da9d6773b6d70ebcbd9c7614c417148f198744d03471cb97d664e40b6632d2282636c4fdec6a2974666cea42cc96f2555c5  posix-ptys.patch
"
